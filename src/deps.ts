/**
 * Smart Deno
 * A template project for DENO
 *
 * Copyright (c) 2020 Alessio Saltarin
 * MIT License
 */

export { Application } from "https://deno.land/x/oak/mod.ts";
export { cyan, gray, red, yellow } from "https://deno.land/std@0.78.0/fmt/colors.ts";
export { sprintf } from "https://deno.land/std@0.78.0/fmt/printf.ts";


